export interface Page {
  label: string;
  icon?: JSX.Element;
  route: string;
  disabled: boolean;
}

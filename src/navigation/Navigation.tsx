import { useState } from 'react';
import {
  BrowserRouter as Router, Route, Routes, Link,
} from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import {
  MdVideogameAsset, MdAccountCircle,
} from 'react-icons/md';
import Game from '../pages/game/Game';
import Login from '../pages/login/Login';
import SignIn from '../pages/signin/SignIn';
import './Navigation.css';

import { Page } from './types';

const ROUTES: JSX.Element = (
  <Routes>
    <Route path="/login" element={<Login />} />
    <Route path="/register" element={<SignIn />} />
    <Route path="/play" element={<Game />} />
  </Routes>
);
const PAGES: Page[] = [
  {
    label: 'navigation.play',
    icon: <MdVideogameAsset />,
    route: '/play',
    disabled: false,
  },
];

function Navigation() {
  const [currentPage, setCurrentPage] = useState<string>('/login');
  const { t } = useTranslation();

  return (
    <Router>
      <div className="navigation">
        <div className="navigation__navigator">
          <div>
            <Link to="/home" onClick={() => setCurrentPage('/home')}>
              <img src="/logo.png" alt="logo" />
            </Link>
            <hr />
            <div className="navigation__navigator__menu">
              {PAGES.map((page: Page) => (
                <Link to={page.route} onClick={() => setCurrentPage(page.route)}>
                  <div
                    className={`navigation__navigator__item${
                      currentPage === page.route ? ' active' : ''
                    }`}
                  >
                    <div className="navigation__navigator__item__icon">
                      {page.icon}
                    </div>
                    <div className="navigation__navigator__item__label">
                      {t(page.label)}
                    </div>
                  </div>
                </Link>
              ))}
            </div>
          </div>
          <div>
            <hr />
            <Link to="/login" onClick={() => localStorage.removeItem('user')}>
              <div
                className={`navigation__navigator__item${
                  currentPage === '/account' ? ' active' : ''
                }`}
              >
                <div className="navigation__navigator__item__icon">
                  <MdAccountCircle />
                </div>
                <div className="navigation__navigator__item__label">
                  {t('navigation.account')}
                </div>
              </div>
            </Link>
          </div>
        </div>
        <div className="navigation__content">{ROUTES}</div>
      </div>
    </Router>
  );
}

export default Navigation;

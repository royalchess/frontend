import { useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import './Home.css';
import SERVER_BASE_URL from '../../config';

interface Lesson {
  title: string,
  preview: string,
  url: string,
  image: string,
}

function Home() {
  const [lessons, setLessons] = useState<Lesson[]>();
  const t = useTranslation();

  useEffect(() => {
    fetch(`${SERVER_BASE_URL}/lessons/${t.i18n.language}`)
      .then((response) => {
        response.json().then((data) => {
          setLessons(data.lesson);
        });
      });
  }, [t.i18n.language]);

  return (
    <div className="home">
      <div className="home__title">Accueil</div>
      { lessons
        ? (
          lessons.map((lesson: Lesson) => (
            <div className="home__lesson" key={lesson.title}>
              <div className="home__lesson__title">
                {lesson.title}
              </div>
              <div className="home__lesson__preview">
                {lesson.preview}
              </div>
              <div className="home__lesson__image">
                <a href={lesson.url}>
                  <img src={lesson.image} alt="lesson" />
                </a>
              </div>
            </div>
          ))
        )
        : (
          <div>No lesson</div>
        )}
    </div>
  );
}

export default Home;

// import { useTranslation } from 'react-i18next';
import {
  withFormik, FormikProps, Form, Field,
} from 'formik';

// import * as Yup from 'yup';
// import { useNavigate } from 'react-router-dom';

import { login } from '../../services/authServices';
// Shape of form values
interface FormValues {
  username: string;
  password: string;
}

interface OtherProps {
  message: string;
}

function InnerForm(props: OtherProps & FormikProps<FormValues>) {
  const {
    touched, errors, message,
  } = props;
  console.log(message);
  return (
    <div className="container px-6 mx-auto flex h-screen justify-center items-center">
      <div className="w-full md:w-full lg:w-4/12 mx-auto md:mx-0">
        <div className="bg-white p-10 flex flex-col w-full rounded-xl border-solid border-2">
          <h1 className="text-3xl font-bold text-gray-600 text-left mb-6">
            RoyalChess
          </h1>
          <h1 className="text-2xl font-bold text-gray-600 text-left mb-4">
            Connection à RoyalChess
          </h1>
          <Form className="mt-6">
            <Field
              id="username"
              name="username"
              className="block w-full px-3 py-4 placeholder-slate-300 text-slate-600 mt-2 text-black-700
                placeholder
                gray-400 bg-white border rounded-md focus:border-black-400 focus:ring-black-300
                focus:outline-none focus:ring focus:ring-opacity-40"
            />
            {touched.username && errors.username && <div>{errors.username}</div>}

            <Field
              type="password"
              name="password"
              className="block w-full px-3 py-4 placeholder-slate-300 text-slate-600 mt-2 text-black-700
                placeholder
                gray-400 bg-white border rounded-md focus:border-black-400 focus:ring-black-300
                focus:outline-none focus:ring focus:ring-opacity-40"
            />
            {touched.password && errors.password && <div>{errors.password}</div>}

            <button
              type="submit"
              className="text-white bg-blue-700 w-full hover:bg-blue-800 focus:ring-4
            focus:outline-none focus:ring-blue-300 font-medium rounded-full text-base px-6 py-3.5 text-center
            dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
            >
              Submit
            </button>
          </Form>
        </div>
      </div>
    </div>
  );
}

// The type of props MyForm receives
interface MyFormProps {
  initialUsername?: string;
  message: string; // if this passed all the way through you might do this or make a union type
}

// Wrap our form with the withFormik HoC
const MyForm = withFormik<MyFormProps, FormValues>({
  // Transform outer props into form values
  mapPropsToValues: (props) => ({
    username: props.initialUsername || '',
    password: '',
  }),

  handleSubmit: (values) => {
    login(values.username, values.password).then(
      () => {
        console.log('pas problem');
        window.location.replace('/play');
      },
      (error : any) => {
        const resMessage = (error.response
                && error.response.data
                && error.response.data.message)
              || error.message
              || error.toString();
        console.log(resMessage);

        // setLoading(false);
        // setMessage(resMessage);
      },
    );
    console.log('ok');
  },
})(InnerForm);
function LogIn() {
  // const navigate = useNavigate();
  return (
    <div>
      <MyForm message="Sign up" />
    </div>
  );
}

export default LogIn;

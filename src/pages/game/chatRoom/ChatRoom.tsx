// import {
//   useState, useEffect,
// } from 'react';

// import {
//   useEffect,
// } from 'react';

import { Chessboard } from 'react-chessboard';

import {
  withFormik, FormikProps, Form, Field,
} from 'formik';

// interface Chatter {
//   name: string;
// }

// interface Msg {
//   username: string;
//   content:string;
// }
interface ChildProps {
  setMode: React.Dispatch<React.SetStateAction<string | undefined>>;
}

// const MSGS: Msg[] = [
//   { username: 'Nicolas', content: 'test avec des donnees du front' }, // add order
//   { username: 'lntdred', content: 'Sa marche' },
//   { username: 'Nicolas', content: 'Il faut implementé sa avc les sockets et on st bien' },
// ];

// const CHATTERS: Chatter[] = [
//   { name: 'Nicolas' },
//   { name: 'lntdred' },
//   { name: 'bigcoce' },
// ];

interface FormValuesMsg {
  msg: string;
}

interface MyFormPropsMsg {
  initialMsg?: string;
}

function InnerFormMsg(props: FormikProps<FormValuesMsg>) {
  const {
    touched, errors,
  } = props;
  return (
    <Form>
      <Field type="msg" name="msg" className="block w-full px-3 py-4 placeholder-slate-300 text-slate-600 mt-2 text-black-700 placeholder-gray-400 bg-white border rounded-md focus:border-black-400 focus:ring-black-300 focus:outline-none focus:ring focus:ring-opacity-40" />
      {touched.msg && errors.msg && <div>{errors.msg}</div>}
      <br />
      <button type="submit" className="text-white bg-slate-400 w-full hover:bg-slate-500 focus:ring-4 focus:outline-none focus:ring-slate-500 font-medium rounded- text-base px-6 py-3.5 text-center">
        Send
      </button>
    </Form>
  );
}

function Board({ setMode }: ChildProps) {
  // const [messages, setMessages] = useState();
  // const [users, setUsers] = useState();
  // console.log(messages);
  // console.log(users);
  const MyFormMsg = withFormik<MyFormPropsMsg, FormValuesMsg>({

    handleSubmit: (values) => {
      // const messageSend: any = {
      //   roomId: 'global',
      //   username: 'lntdred',
      //   avatar: 'lnt',
      //   content: values.msg,
      // };
      // console.log(socket);
      // console.log(messageSend);
      // eslint-disable-next-line react/destructuring-assignment
      // socket.socket.emit('exchanges', messageSend);
      console.log(values);
    },
  })(InnerFormMsg);

  // useEffect(() => {
  //   const messageListener = (message: any) => {
  //     setMessages((prevMessages: any) => {
  //       const newMessages = { ...prevMessages };
  //       newMessages[message.id] = message;
  //       return newMessages;
  //     });
  //   };

  //   const userListener = (user: any) => {
  //     setUsers(user);
  //   };

  //   socket.socket.on('global', messageListener);
  //   socket.socket.on('participants/global', userListener);

  //   return () => {
  //     socket.socket.off('message', messageListener);
  //     socket.socket.off('deleteMessage', userListener);
  //   };
  // }, [socket]);

  // eslint-disable-next-line react/destructuring-assignment
  // socket.socket.on('participants/global', (...args: any) => {
  //   console.log(args);
  // });

  // const [users, setUsers] = useState([]);
  // console.log(socket);

  // useEffect(() => {
  //   fetch('https://royal-chess-backend.herokuapp.com/api/v1/rooms/global/messages', {
  //     method: 'GET',
  //     body: JSON.stringify({
  //       roomId: 'global',
  //       fromIndex: 1,
  //       toIndex: 3,
  //     }),
  //   })
  //     .then((res) => {
  //       if (!res.ok) throw new Error(res.statusText);
  //       return res.json();
  //     })
  //     .then((res) => console.log(res))
  //     .catch(console.log);
  // }, []);

  //   useEffect(() => {
  //     console.log('ALERTE');
  //     console.log(socket);
  //     // eslint-disable-next-line react/destructuring-assignment
  //     // socket.socket.on('global', (data: never) => setMessages([...messages, data]));
  //     socket.socket.on('global', (data: never) => console.log(data));

  //     console.log(socket);
  //   }, [socket]);

  const endForfeit = () => {
    setMode(undefined!);
  };

  return (
    <div>
      <div className="game">
        <div>
          <div className="mode-picker__label">Sandbox</div>
          <br />
          <Chessboard id={2} />
        </div>
        <div>
          <div className="mode-picker__label">Chat room</div>
          <br />
          <div className="chat-box">
            <div className="chat-box-top">
              <div className="chat-box-msg">
                <div className="chat-box-msg__title">
                  Message
                </div>
                {/* {MSGS.map((mode: Msg) => (
                  <div className="chat-box-msg__label">
                    {mode.username}
                    :
                    {mode.content}
                  </div>
                ))} */}
              </div>
              <div className="chat-box-chatters">
                <div className="chat-box-users__title">
                  Current Users
                </div>
                {/* {CHATTERS.map((mode: Chatter) => (
                  <div className="chat-box-users__label">
                    {mode.name}
                  </div>
                ))} */}
              </div>
              <div className="chat-box-text">
                <MyFormMsg />
              </div>
            </div>
          </div>
        </div>
        <div className="back-button">
          <button type="button" onClick={() => endForfeit()}>
            Retour séléction
          </button>
        </div>
      </div>
    </div>
  );
}

export default Board;

import { useState, useEffect } from 'react';
import { Chessboard } from 'react-chessboard';
import {
  withFormik, FormikProps, Form, Field,
} from 'formik';
import { io } from 'socket.io-client';
import { createRoom } from '../../../services/roomServices';

interface FormValuesCreate {
  roomname: string
}

interface MyFormPropsCreate {
  initialRoomname?: string
}

interface Msg {
  order: number
  username: string
  content: string
}

interface FormValuesMsg {
  msg: string
}

interface MyFormPropsMsg {
  initialMsg?: string
}

function InnerFormMsg(props: FormikProps<FormValuesMsg>) {
  const { touched, errors } = props;
  return (
    <Form>
      <Field
        type="msg"
        name="msg"
        className="block w-full px-3 py-4 placeholder-slate-300 text-slate-600 mt-2 text-black-700 placeholder-gray-400 bg-white border rounded-md focus:border-black-400 focus:ring-black-300 focus:outline-none focus:ring focus:ring-opacity-40"
      />
      {touched.msg && errors.msg && <div>{errors.msg}</div>}
      <br />
      <button
        type="submit"
        className="text-white bg-slate-400 w-full hover:bg-slate-500 focus:ring-4 focus:outline-none focus:ring-slate-500 font-medium rounded- text-base px-6 py-3.5 text-center"
      >
        Send
      </button>
    </Form>
  );
}

function InnerFormCreate(props: FormikProps<FormValuesCreate>) {
  const { touched, errors } = props;
  return (
    <Form>
      <Field
        type="roomname"
        name="roomname"
        className="block w-full px-3 py-4 placeholder-slate-300 text-slate-600 mt-2 text-black-700 placeholder-gray-400 bg-white border rounded-md focus:border-black-400 focus:ring-black-300 focus:outline-none focus:ring focus:ring-opacity-40"
      />
      {touched.roomname && errors.roomname && <div>{errors.roomname}</div>}
      <br />

      <button
        type="submit"
        className="text-white bg-slate-400 w-full hover:bg-slate-500 focus:ring-4 focus:outline-none focus:ring-slate-500 font-medium rounded-full text-base px-6 py-3.5 text-center"
      >
        Create
      </button>
    </Form>
  );
}

const MyFormCreate = withFormik<MyFormPropsCreate, FormValuesCreate>({
  // Transform outer props into form values
  mapPropsToValues: (props) => ({
    roomname: props.initialRoomname || '',
  }),

  handleSubmit: (values) => {
    console.log(values);
    createRoom(values.roomname, localStorage.getItem('user')!);
  },
})(InnerFormCreate);

// eslint-disable-next-line @typescript-eslint/no-unused-vars
function ModePicker() {
  const [roomsInfo, setRoomsInfo] = useState(['global']);
  const [mode, setMode] = useState<string>();
  const [socket, setSocket] = useState<any>();
  const [users, setUsers] = useState<string[]>();
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [messages, setMessages] = useState<Msg[]>([
    {
      order: 0,
      username: 'server',
      content: 'connected to the room',
    },
  ]);
  const endForfeit = () => {
    socket.on('disconnect', () => {});
    setMode(undefined!);
  };

  console.log(users);

  const MyFormMsg = withFormik<MyFormPropsMsg, FormValuesMsg>({
    handleSubmit: (values) => {
      console.log(localStorage.getItem('user'));
      console.log(socket);
      const messageSend: any = {
        roomId: mode,
        username: localStorage.getItem('user'),
        avatar: 'lnt',
        content: values.msg,
      };
      socket?.emit('exchanges', messageSend);
      console.log('sa marche?');
      console.log(values);
    },
  })(InnerFormMsg);

  const userListener = (user: any) => {
    setUsers(user);
  };

  const messageListener = (message: Msg) => {
    const newMessages = [...messages, message];
    setMessages(newMessages);
    console.log(message);
    console.log(messages);
  };

  socket?.on(mode, messageListener);
  socket?.on(`participants/${{ mode }}`, userListener);

  const handleClickMode = (roomName: string) => {
    const newSocket = io('https://royal-chess-backend.herokuapp.com/');
    setSocket(newSocket);
    socket.on('connect', () => {
      console.log('socket');
    });
    console.log(localStorage.getItem('user'));
    const message: any = {
      roomId: roomName,
      username: JSON.parse(localStorage.getItem('user')!),
      avatar: 'lnt',
      connected: true,
    };
    socket?.emit('participants', message);
    setMode(roomName);
  };

  useEffect(() => {
    fetch('https://royal-chess-backend.herokuapp.com/api/v1/rooms', {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('accessToken')!)}`,
      },
    })
      .then((res) => {
        if (!res.ok) throw new Error(res.statusText);
        return res.json();
      })
      .then((res) => setRoomsInfo(res))
      .catch(console.log);
  }, []);

  if (!mode) {
    return (
      <div className="game">
        <div>
          <div className="mode-picker__label">Choose chat room</div>
          <br />
          <div className="mode-picker">
            {roomsInfo.map((room: string) => (
              <button
                type="button"
                className="mode-picker__choice"
                onClick={() => handleClickMode(room)}
              >
                {room}
              </button>
            ))}
          </div>
          <div className="mode-picker__label">Create your own chat room</div>
          <br />
          <div className="mode-picker-2">
            <MyFormCreate />
          </div>
        </div>
      </div>
    );
  }
  return (
    <div>
      <div className="game">
        <div>
          <div className="mode-picker__label">Sandbox</div>
          <br />
          <Chessboard id={2} />
        </div>
        <div>
          <div className="mode-picker__label">Chat room</div>
          <br />
          <div className="chat-box">
            <div className="chat-box-top">
              <div className="chat-box-msg">
                <div className="chat-box-msg__title">Message</div>
                {messages?.map((msgs: Msg) => (
                  <div className="chat-box-msg__label">
                    {msgs.username}
                    :
                    {msgs.content}
                  </div>
                ))}
              </div>
              <div className="chat-box-chatters">
                <div className="chat-box-users__title">Current Users</div>
                {/* {CHATTERS.map((mode: string) => (
                  <div className="chat-box-users__label">
                    {mode.name}
                  </div>
                ))} */}
              </div>
              <div className="chat-box-text">
                <MyFormMsg />
              </div>
            </div>
          </div>
        </div>
        <div className="back-button">
          <button type="button" onClick={() => endForfeit()}>
            Retour séléction
          </button>
        </div>
      </div>
    </div>
  );
}
export default ModePicker;

import './Game.css';

import ModePicker from './selectRoom/SelectRoom';

function Game() {
  // const socket = io('https://royal-chess-backend.herokuapp.com/');

  // socket.on('connect', () => {
  //   console.log('socket');
  // });
  return (
    <ModePicker />
  );
}
export default Game;

import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

import Backend from 'i18next-http-backend';
import LanguageDetector from 'i18next-browser-languagedetector';

import EnLocales from './locales/en/translation.json';
import FrLocales from './locales/fr/translation.json';

const resources = {
  en: {
    translation: EnLocales,
  },
  fr: {
    translation: FrLocales,
  },
};

i18n
  .use(Backend)
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    resources,
    fallbackLng: 'en',
    debug: true,

    interpolation: {
      escapeValue: false,
    },
  });

export default i18n;

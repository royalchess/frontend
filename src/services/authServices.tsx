import axios from 'axios';
// import { useNavigate } from 'react-router-dom';

export const register = (username: string, email: string, password: string) => axios.post('https://royal-chess-backend.herokuapp.com/users/create', {
  username,
  email,
  password,
});

export const apiLogin = async (username: string, password: string) => {
  const option = {
    method: 'POST',
    headers: {
      'Content-type': 'application/json',
    },
    body: JSON.stringify({
      username,
      password,
    }),
  };
  console.log('enter here');
  const res = await fetch('https://royal-chess-backend.herokuapp.com/auth/login', option);
  const json = await res.json();
  if (res.status !== 200) {
    throw Error(json.message);
  }
  return json;
};

export const login = (username: string, password: string) => axios
  .post('https://royal-chess-backend.herokuapp.com/auth/login', {
    username,
    password,
  }, { headers: { 'Content-Type': 'application/json' } })
  .then((response) => {
    if (response.data.access_token) {
      console.log(response.data);
      localStorage.setItem('user', JSON.stringify(username));
      localStorage.setItem('accessToken', JSON.stringify(response.data.access_token));
    }

    return response.data;
  });

export const logout = () => {
  localStorage.removeItem('user');
};

export const getCurrentUser = () => {
  const userStr = localStorage.getItem('user');
  if (userStr) return JSON.parse(userStr);

  return null;
};

import axios from 'axios';

// export function getRooms() {

//         fetch('https://royal-chess-backend.herokuapp.com/api/v1/rooms', {
//           method: 'GET',
//         })
//           .then((res) => {
//             if (!res.ok) throw new Error(res.statusText);
//             return res.json();
//           })
//           .then((res) => console.log(res))
//           .catch(console.log);
//       }, [userContext]);
//   const { data, status } = axios.get<any>(
//     'https://royal-chess-backend.herokuapp.com/api/v1/rooms',
//     {
//       headers: {
//         Accept: 'application/json',
//       },
//     },
//   );
//   console.log(status);
//   return data;
// }

export const createRoom = (roomId: string, username: string) => axios.post('https://royal-chess-backend.herokuapp.com/api/v1/rooms', {
  roomId,
  username,
}, {
  headers: {
    Authorization: `Bearer ${JSON.parse(localStorage.getItem('accessToken')!)}`,
  },
});

export const deleteRoom = (roomId: string) => axios.delete(`https://royal-chess-backend.herokuapp.com/api/v1/rooms/${roomId}`, {
  data: {
    roomId,
  },
});

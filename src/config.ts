const SERVER_BASE_URL = 'https://royal-chess-backend.herokuapp.com';

export default SERVER_BASE_URL;
